import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class BabysitterTest {
	
	Sitter sitter;
	Sitter customSitter;
	Sitter zeroSitter;
	
	@Before
	public void setUp() {
		sitter = new Sitter();
		customSitter = new Sitter(1,3,10);
	}
	
	@Test
	public void sitterHasDefaultStartTime() {
		assertEquals(0, sitter.getStartTime());
	}
	
	@Test
	public void sitterHasDefaultEndTime() {
		assertEquals(11, sitter.getEndTime());
	}
	
	@Test
	public void sitterHasDefaultBedTime() {
		assertEquals(4, sitter.getBedTime());
	}
	
	@Test
	public void calculateCorrectDefaultAmmountPreBedtime() {
		assertEquals(48, sitter.ammountPreBed());
	}
	
	@Test
	public void calculateCorrectDefaultAmmountPostBedtime() {
		assertEquals(24, sitter.ammountPostBed());
	}
	
	@Test
	public void calculateCorrectDefaultAmmountPostMidnight() {
		assertEquals(64, sitter.ammountPostMidnight());
	}
	
	@Test
	public void calculateDefaultTotalAmmount() {
		assertEquals(136, sitter.ammountTotal());
	}
	
	@Test
	public void sitterHasCorrectValuesWhenInstantiated() {
		assertEquals(1, customSitter.getStartTime());
		assertEquals(3, customSitter.getBedTime());
		assertEquals(10, customSitter.getEndTime());
	}
	
	@Test
	public void calculateCorrectCustomTotal() {
		assertEquals(104, customSitter.ammountTotal());
	}
	
	@Test
	public void calculateCorrectZeroPreBedtimeAmmount() {
		zeroSitter = new Sitter(4,3,10);
		assertEquals(0, zeroSitter.ammountPreBed());
		assertEquals(72, zeroSitter.ammountTotal());
	}
	
	@Test
	public void calculateCorrectZeroPostBedtimeAmmount() {
		zeroSitter = new Sitter(3,8,10);
		assertEquals(0, zeroSitter.ammountPostBed());
		assertEquals(96, zeroSitter.ammountTotal());
	}
	
	@Test
	public void calculateCorrectZeroPostMidnightAmmount() {
		zeroSitter = new Sitter(0,4,7);
		assertEquals(0, zeroSitter.ammountPostMidnight());
		assertEquals(72, zeroSitter.ammountTotal());
	}
	
	@Test
	public void calculateCorrectZeroPostBedtimeAndMidnight() {
		zeroSitter = new Sitter(0,8,7);
		assertEquals(0, zeroSitter.ammountPostBed());
		assertEquals(0, zeroSitter.ammountPostMidnight());
		assertEquals(84, zeroSitter.ammountTotal());
	}
	
	@Test
	public void calculateCorrectZeroPostBedtimeAmmountWhenEndTimeIsBeforeBedtime() {
		zeroSitter = new Sitter(0,6,5);
		assertEquals(0, zeroSitter.ammountPostBed());
		assertEquals(60, zeroSitter.ammountTotal());
	}
	
	@Test
	public void calculateCorrectAmmountWhenBedTimeIsAfterEndAndEndIsPostMidnight() {
		zeroSitter = new Sitter(4,9,8);
		assertEquals(0, zeroSitter.ammountPostBed());
		assertEquals(52, zeroSitter.ammountTotal());
	}
	
	@Test
	public void calculateCorrectZeroAmmountWhenStartIsAfterEnd() {
		zeroSitter = new Sitter(7,4,6);
		assertEquals(0, zeroSitter.ammountTotal());
	}
	
	@Test
	public void calculateCorrectAmmountWhenStartIsAfterBedAndMidnight() {
		zeroSitter = new Sitter(7,4,10);
		assertEquals(0, zeroSitter.ammountPreBed());
		assertEquals(0, zeroSitter.ammountPostBed());
		assertEquals(48, zeroSitter.ammountTotal());
	}
	
	@Test
	public void calculateTotalWhenBedBeforeStartAndEndBeforeMidnight() {
		zeroSitter = new Sitter(4,3,7);
		assertEquals(24, zeroSitter.ammountTotal());
	}
	
	@Test
	public void calculateTotalWhenStartAndBedtimeAfterMidnight() {
		zeroSitter = new Sitter(7,8,10);
		assertEquals(48, zeroSitter.ammountTotal());
	}
	
	@Test
	public void calculateTotalWhenStartAfterMidnightAndEndBeforeBed() {
		zeroSitter = new Sitter(7,11,10);
		assertEquals(48, zeroSitter.ammountTotal());
	}
	
	@Test
	public void calculateTotalWhenBedAfterMidnightAndStartAfterBed() {
		zeroSitter = new Sitter(9,8,10);
		assertEquals(0, zeroSitter.ammountPreBed());
		assertEquals(0, zeroSitter.ammountPostBed());
		assertEquals(16, zeroSitter.ammountTotal());
	}

}
