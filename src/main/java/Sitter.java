
public class Sitter {
	
	// Start 0 (5PM), Bed 4 (9PM), Midnight 7 (12PM), End 11 (4AM)

	private int startTime = 0;
	private int bedTime = 4;
	private int endTime = 11;
	
	public Sitter() {
		
	}
	
	public Sitter(int startTime, int bedTime, int endTime) {
		this.startTime = startTime;
		this.bedTime = bedTime;
		this.endTime = endTime;
	}

	public int getStartTime() {
		return startTime;
	}

	public int getEndTime() {
		return endTime;
	}

	public int getBedTime() {
		return bedTime;
	}

	public int ammountPreBed() {
		if (bedTime < endTime) {
			if (bedTime > startTime && bedTime <= 7) {
				return (bedTime - startTime) * 12;
			} else if (bedTime > startTime && bedTime > 7) {
				return (7 - startTime) * 12;
			} else {
				return 0;
			}
		} else {
			if (endTime < 7) {
				return (endTime - startTime) * 12;
			} else {
				return (7 - startTime) * 12;
			}
		}
	}

	public int ammountPostBed() {
		if (bedTime < endTime) {
			if (bedTime < startTime && startTime < 7) {
				return (7 - startTime) * 8;
			} else if (bedTime < 7 && startTime < 7) {
				return (7 - bedTime) * 8;
			} else {
				return 0;
			}
		} else {
			return 0;
		}
	}

	public int ammountPostMidnight() {
		if (endTime > 7) {
			if (startTime < 7) {
				return (endTime - 7) * 16;
			} else {
				return (endTime - startTime) * 16;
			}
		} else {
			return 0;
		}
	}

	public int ammountTotal() {
		if (startTime < endTime) {
			return this.ammountPreBed() + this.ammountPostBed() + this.ammountPostMidnight();
		} else {
			return 0;
		}
	}
}
